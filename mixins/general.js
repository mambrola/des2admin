export default {
  data() {
    return {
    }
  },
  computed: {
    $vc() {
      const mode = this.$vuetify.theme.dark ? 'dark' : 'light'
      return this.$vuetify.theme.themes[mode]
    }
  },
  methods: {
    $stageColor(stage) {
      return stage > this.$stages.break_noMoney
        ? 'des_iconBtn'
        : stage >= this.$stages.CHOICED
          ? 'des_iconBtnBG'
          : stage >= this.$stages.RESPONSED
            ? 'des_green'
            : stage >= this.$stages.OFFERED
              ? 'des_yellow'
              : 'des_iconBtn'
    },
    $stageBGColor(stage) {
      return stage > this.$stages.break_noMoney
        ? 'des_iconBtnBG'
        : stage >= this.$stages.CHOICED
          ? 'des_iconBtn'
          : stage >= this.$stages.RESPONSED
            ? 'des_greenBG'
            : stage >= this.$stages.OFFERED
              ? 'des_yellowBG'
              : 'des_iconBtnBG'
    }
  }
}
