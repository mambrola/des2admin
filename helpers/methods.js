import jwt from 'jsonwebtoken'

export const mapSync = (data) => {
  const result = {}
  for (const [key, prop] of Object.entries(data)) {
    result[key] = {
      get() {
        return this[prop]
      },
      set(val) {
        this.$emit('update:' + prop, val)
      }
    }
  }
  return result
}

export const getjwtMidForRecId = function (recId) {
  const token = jwt.sign(
    { recId, iat: Math.round(0.001 * Date.now()) },
    process.env.JWT_SECRET
  )
  const tokens = token.split('.')
  return tokens[1] + '.' + tokens[2]
}

export const copyToClipboard = function (id, doc, win) {
  const range = doc.createRange()
  range.selectNode(doc.getElementById(id))
  win.getSelection().removeAllRanges()
  win.getSelection().addRange(range)
  doc.execCommand('copy')
  win.getSelection().removeAllRanges()
}