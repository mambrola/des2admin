import colors from './desColors'

const dark = {
  des_primary: colors.common.des_main_blue,
  des_secondary: colors.common.des_main_red
}
const light = {
  des_primary: colors.common.des_main_red,
  des_secondary: colors.common.des_main_blue
}
Object.keys(colors.greys).forEach(function (color) {
  dark[color] = colors.greys[color].dark
  light[color] = colors.greys[color].light
})

export default { dark, light }
