export default {
  common: {
    des_main_red: '#ec1c24',
    des_main_blue: '#2e74e5',
    des_text_dark: '#ob2041',
    des_text_light: '#f5f8fe'
  },
  greys: {
    // ფონის ფერები
    des_grey14F0: { dark: '#141414', light: '#F0F0F0' },
    des_grey1ED2: { dark: '#1E1E1E', light: '#D2D2D2' },
    des_grey23DC: { dark: '#232323', light: '#DCDCDC' },
    des_grey28F0: { dark: '#282828', light: '#F0F0F0' },
    des_grey28FF: { dark: '#282828', light: '#FFFFFF' },
    des_grey32FF: { dark: '#323232', light: '#FFFFFF' },
    des_grey46C8: { dark: '#464646', light: '#C8C8C8' },
    des_grey7F7F: { dark: '#7F7F7F', light: '#7F7F7F' },
    des_greyFF00: { dark: '#FFFFFF', light: '#000000' },
    des_gradient: { dark: '#000000', light: '#FFFFFF' },
    // font-ის ფერები
    des_fontLily: { dark: '#F5F8FE', light: '#F5F8FE' },
    des_fontBlue: { dark: '#0B2041', light: '#0B2041' },
    des_fontLiBl: { dark: '#F5F8FE', light: '#0B2041' },
    des_fontLiBl_lighten: { dark: '#A1A3A9', light: '#153B77' },
    des_fontBlLi: { dark: '#0B2041', light: '#F5F8FE' },

    des_bgLily: { dark: '#FAFCFF', light: '#FAFCFF' },
    des_bgBlue: { dark: '#010409', light: '#010409' },
    des_bgLiBl: { dark: '#FAFCFF', light: '#010409' },
    des_bgBlLi: { dark: '#010409', light: '#FAFCFF' },

    // suit-ის ფერები
    blt_spade: { dark: '#00001A', light: '#00001A' },
    blt_heart: { dark: '#990000', light: '#990000' },
    blt_diamond: { dark: '#FF0000', light: '#FF0000' },
    blt_club: { dark: '#001A00', light: '#001A00' },
    blt_spade_back: { dark: '#E0E0EB', light: '#E0E0EB' },
    blt_heart_back: { dark: '#EBDFDF', light: '#EBDFDF' },
    blt_diamond_back: { dark: '#EAE3DF', light: '#EAE3DF' },
    blt_club_back: { dark: '#E0ECE0', light: '#E0ECE0' },
    blt_nt: { dark: '#010201', light: '#010201' },
    blt_nt_back: { dark: '#FDFCFD', light: '#FDFCFD' },
    // pass, double-redouble
    blt_pass: { dark: '#FFFFFF', light: '#FFFFFF' },
    blt_x: { dark: '#FFFFFF', light: '#FFFFFF' },
    blt_xx: { dark: '#FFFFFF', light: '#FFFFFF' },
    blt_pass_back: { dark: '#006600', light: '#006600' },
    blt_x_back: { dark: '#9C0000', light: '#9C0000' },
    blt_xx_back: { dark: '#3434EB', light: '#3434EB' },

    // სახელობითი ფერები
    des_iconBtn: { dark: '#023047', light: '#023047' },
    des_iconBtnBG: { dark: '#ECEFF1', light: '#ECEFF1' },
    des_yellow: { dark: '#F57F17', light: '#F57F17' },
    des_yellowBG: { dark: '#FFEE58', light: '#FFEE58' },
    des_green: { dark: '#00695C', light: '#00695C' },
    des_greenBG: { dark: '#26a69a', light: '#26a69a' },
    des_red: { dark: '#B71C1C', light: '#B71C1C' },
    des_redBG: { dark: '#EF5350', light: '#EF5350' },

    no_testers: { dark: '#eeeeee', light: '#eeeeee' },
    no_offered: { dark: '#cccccc', light: '#cccccc' },
    offered: { dark: '#ffb703', light: '#ffb703' },
    responsed: { dark: '#2a9d8f', light: '#2a9d8f' },
    choicing: { dark: '#118ab2', light: '#118ab2' },
    choiced: { dark: '#023047', light: '#1d3557' },
    asked_cancel: { dark: '#c53b5c', light: '#c53b5c' },
    canceled: { dark: '#c53b5c', light: '#c53b5c' },
    resulted: { dark: '#023047', light: '#023047' },
    paypal_blue1: { dark: '#00457C', light: '#00457C' },
    paypal_blue2: { dark: '#0079C1', light: '#0079C1' },
    paypal_gold: { dark: '#ffd700', light: '#ffd700' },
    // coolors-ის ფერები
    des_blue: { dark: '#003049', light: '#003049' },
    des_biege: { dark: '#eae2b7', light: '#eae2b7' },

    // გიას ფერები
    des_grey1de3: { dark: '#1d1d1d', light: '#e3e3e3' },
    des_grey1dea: { dark: '#1d1d1d', light: '#eaeaea' },
    des_grey1de5: { dark: '#1d1d1d', light: '#e5e5e5' },
    des_grey11ee: { dark: '#111111', light: '#eeeeee' },
    des_grey11f4: { dark: '#111111', light: '#f4f4f4' },
    des_grey19f0: { dark: '#191919', light: '#f0f0f0' },
    des_grey9582: { dark: '#959595', light: '#828282' },
    des_grey28ee: { dark: '#282828', light: '#eeeeee' },
    des_grey28d7: { dark: '#282828', light: '#d7d7d7' },
    des_grey5fc7: { dark: '#5f5f5f', light: '#c7c7c7' },
    des_greyc75f: { dark: '#c7c7c7', light: '#5f5f5f' },
    des_redblue52e: { dark: '#e53a2e', light: '#2e74e5' },
    des_bluered2ee5: { dark: '#2e74e5', light: '#e53a2e' },
    des_blueblue: { dark: '#2e74e5', light: '#2e74e5' },
    des_grey32cd: { dark: '#323232', light: '#cdcdcd' },
    des_gold: { dark: '#a67c00', light: '#a67c00' },
    message_bg_me: { dark: '#008055', light: '#008055' },
    message_txt_me: { dark: '#f2f2f2', light: '#f2f2f2' },
    sysmessage_bg: { dark: '#5f5f5f', light: '#5f5f5f' },
    sysmessage_txt: { dark: '#f2f2f2', light: '#f2f2f2' },
    file_layout_bg: { dark: '#5f5f5f', light: '#5f5f5f' },
    file_layout_txt: { dark: '#f2f2f2', light: '#f2f2f2' },
    message_bg_he: { dark: '#003366', light: '#003366' },
    message_txt_he: { dark: '#f2f2f2', light: '#f2f2f2' },
    message_is_received: { dark: '#3333ff', light: '#3333ff' },
    message_not_received: { dark: '#e6f2ff', light: '#e6f2ff' },

    // information
    info_back: { dark: '#ffffbd', light: '#ffffbd' }
  }
}
