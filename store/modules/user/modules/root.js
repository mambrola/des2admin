const initialState = {
  userId: null
}

export default {
  state: () => {
    return Object.assign(
      {
        locale: navigator.language.slice(0, 2),
        darkMode: false
      },
      initialState
    )
  },
  mutations: {
    setLocale(state, params) {
      params.i18n.locale = state.locale = params.locale
    }
  }
}