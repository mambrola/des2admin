import root from './modules/root'

export default {
  modules: { root }
}
