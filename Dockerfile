FROM node:14.18

# create app directory in container
WORKDIR /usr/src/app 

# copy package.json into working directory
COPY package*.json ./ 

# install all necessary packages for production
RUN npm install --only=production
RUN npm audit fix

# copy all code files into working directory
COPY . /usr/src/app/  

# generate files, spa with prerendered sites
RUN npm run build
EXPOSE 80
# finally serve the site contents
CMD [ "npm", "run", "start" ] 
