const getOrderStages = async function (axios, app) {
  let value = {}
  try {
    value = (await axios.get('/api/general/orderStages', { headers: { token: process.env.DES_BACK_TOKEN } })).data
  } catch (err) {
    app.$getLapseErrorTextOrLogout('constantsFromDB', '$axios.get: /api/general/orderStages', err)
  }
  return value
}

export default async function ({ store, $axios, app }, inject) {
  inject('stages', await getOrderStages($axios, store, app))
}
