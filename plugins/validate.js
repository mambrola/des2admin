import validate from 'des-validate'

export default ({ app }, inject) => {
  inject('vr', validate(app.i18n.t.bind(app.i18n)))
}
