import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default ({ app, store }) => {
  app.i18n = new VueI18n({
    locale: store.state.user.root.locale,
    fallbackLocale: 'ka',
    messages: {
      ka: require('~/locales/ka.json'),
      de: require('~/locales/de.json')
    }
  })
}
