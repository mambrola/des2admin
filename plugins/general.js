import Vue from 'vue'
import publicIp from 'public-ip'
import General from '@/mixins/general'

Vue.mixin(General)

const getInner = async function (store) {
  const ip = await publicIp.v4()
  return JSON.stringify({
    ip,
    userId: store.state.user.root.userId,
    locale: store.state.user.root.locale
  })
}


export default async function ({ store, $axios, app }, inject) {
  inject('inner', await getInner(store))
}
